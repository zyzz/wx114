import smtplib
import threading
from email.mime.text import MIMEText

import requests
from bs4 import BeautifulSoup

headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.8,en-us;q=0.6,en;q=0.5;q=0.4',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36 MicroMessenger/6.5.2.501 NetType/WIFI WindowsWechat QBCore/3.43.373.400 QQBrowser/9.0.2524.400'
}

# 通过fiddler抓取看下自己的cookies信息填入到下面
cookies = {
    'JSESSIONID': '',
    'SESSION_COOKIE': '',
    'Hm_lvt_dbbb60c103be9716caf0efcce556046d': '',
    'Hm_lpvt_dbbb60c103be9716caf0efcce556046d': ''
}

# 发送方的email信息
FROM = 'hao.he@foxmail.com'
server = smtplib.SMTP()
server.connect('smtp.qq.com', 25)
server.starttls()
server.login(FROM, '')  # 这里设置邮箱的授权码， 注意不是邮箱密码

msg = MIMEText('快抢快抢快抢！')
msg["Subject"] = '有号了！！！有号了！！！'
msg["From"] = FROM
msg["To"] = 'hao.he@foxmail.com'


def fun_timer():
    r = requests.get(
        'http://yyghwx.bjguahao.gov.cn/common/dutysource/appoints/133,200001345.htm?departmentName=%25E7%2589%2599%25E4%25BD%2593%25E7%2589%2599%25E9%25AB%2593&type=1',
        headers=headers, cookies=cookies)

    bsObj = BeautifulSoup(r.text, "html.parser")
    a = bsObj.find_all('a', class_='data_bgcol')
    if a:
        server.sendmail(FROM, ['hao.he@foxmail.com'], msg.as_string())  # 填写收件方
        return

    global timer
    timer = threading.Timer(60 * 1, fun_timer)  # 这里自己调整时间间隔，默认1分钟查询一次
    timer.start()


timer = threading.Timer(1, fun_timer)
timer.start()
